/* eslint-disable import/no-commonjs */
/* eslint-disable import/no-extraneous-dependencies */

const fs = require('fs');

function createSymlinks(env) {
// Create ./dist/img/lg/* symlinks only if images need translation
  if (fs.existsSync(`${env.img.assets}fr`)) {
    if (!fs.existsSync(env.img.dist)) {
      fs.mkdirSync(env.img.dist, { recursive: true });
    }

    for (let i = 0; i < env.translations.available.length; i += 1) {
      if (!fs.existsSync(`${env.img.dist}${env.translations.available[i]}`)) {
        fs.mkdirSync(`${env.img.dist}${env.translations.available[i]}`);
      }

      fs.readdirSync(`${env.img.assets}fr`).forEach((file) => {
        const symlink = {
          src: `${env.img.assets}${env.translations.available[i]}/${file}`,
          origin: `../fr/${file}`,
          dest: `${env.img.dist}${env.translations.available[i]}/${file}`,
        };
        if (!fs.existsSync(symlink.src) && !fs.existsSync(symlink.dest)) {
          fs.symlink(symlink.origin, symlink.dest,
            (err) => { console.log(err) }); // eslint-disable-line
        }
      });
    }
  }

  // Create ./dist/img/opengraph/*.jpg symlinks for each page
  if (fs.existsSync(`${env.img.assets}opengraph/home.jpg`)) {
    if (!fs.existsSync(`${env.img.dist}opengraph`)) {
      fs.mkdirSync(`${env.img.dist}opengraph`, { recursive: true });
    }

    for (let i = 0; i < env.views.length; i += 1) {
      const symlink = {
        src: `${env.img.assets}opengraph/${env.views[i].toLowerCase()}.jpg`,
        origin: '../opengraph/home.jpg',
        dest: `${env.img.dist}opengraph/${env.views[i].toLowerCase()}.jpg`,
      };
      if (!fs.existsSync(symlink.src) && !fs.existsSync(symlink.dest)) {
        fs.symlink(symlink.origin, symlink.dest,
          (err) => { console.log(err) }); // eslint-disable-line
      }
    }
  }
}

module.exports = createSymlinks;
