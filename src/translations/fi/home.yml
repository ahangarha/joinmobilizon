title: Eiköhän oteta omat tapahtumat takaisin omaan hallintaan
subtitle: Yhdessä saavutamme enemmän
devby: 'Kehittänyt: @:color.soft'
intro:
  tags:
  - Keräännytään
  - Organisoidaan
  - Mobilisoidaan
  support: Tue meitä
  title: Käyttäjäystävällinen, vapauttava ja eettinen työkalu kerääntymiseen, organisointiin
    ja mobilisointiin.
prez:
  tool:
    title: Käytännöllinen työkalu
    md: "Mobilizon on työkalu joka helpottaa **tapahtumien löytämistä, luomista ja\n\
      organisointia**.\n\nVoit myös **luoda sivun ryhmällesi** jossa jäsenet\nvoivat\
      \ **organisoitua yhdessä**."
  alt:
    title: Eettinen vaihtoehto
    md: "Eettisenä Facebookin tapahtumien, ryhmien ja sivujen vaihtoehtona Mobilizon\n\
      on **suunniteltu palvelemaan sinua**. Piste.\n\nEi _tykkäyksi'_, ei _seuraamista_,\
      \ ei ääretöntä määrää _selattavaa_: **Mobilizon\nantaa sinun päättää mihin huomiosi\
      \ suuntaat**."
  soft:
    title: Federoitu ohjelmisto
    md: "Mobilizon ei ole jättimäinen alusta vaan **kokoelma ristiinkytkeytyneitä\
      \ Mobilizon\nverkkosivuja**.\n\nFederoitu arkkitehtuuri **ehkäisee monopoliasetelmia**\
      \ ja tarjoaa\n**monimuotoisen valikoiman käyttöehtoja**."
  btn:
  - Mikä Mobilizon on?
  - Aloita mobilizon.org:issa
what:
  event:
    title: Tapahtumasi
    md: "Mobilizonissa voit **luoda yksityiskohtaisen sivun tapahtumallesi**,\nsekä\
      \ julkaista ja jakaa sen.\n\nVoit myös etsiä tapahtumia avainsanoilla, paikalla\
      \ tai päivämäärällä, **osallistua tapahtumiin (jopa ilman käyttäjätiliä)** ja\
      \ lisätä niitä aikatauluusi."
  profile:
    title: Profiilisi
    md: "Tilin luominen Mobilizoniin tarkoittaa että voit\n**luoda useita profiileja**\
      \ (esim. henkilökohtaisen, työprofiilin, harrastusprofiilin, aktivismiprofiilin,\n\
      jne.), **organisoida tapahtumia** ja **hallinnoida ryhmiä**.\n\nEnnen tilin\
      \ luomista instanssille, muista **tutustua instanssiin lukemalla sen \"tietoja\"\
      \ -sivu**, niin ymmärrät sen säännöt ja periaatteet."
  group:
    title: Ryhmäsi
    md: "Mobilizonissa, **jokaisella ryhmällä on julkinen sivu** jossa voit\ntsekata\
      \ sen viimeisimmät **postaukset ja julkiset tapahtumat**.\n\nKutsuttaessa ryhmään,\
      \ jäsenet voivat **osallistua keskusteluihin\nja muokata yhteisien resurssien\
      \ kansiota** (esim. linkit yhteisiin dokumentteihin,\ntyökaluihin, wikiin, jne.)"
  intro: "## Mikä Mobilizon?\n\nMobilizon on verkkotyökalu jolla voit hallita tapahtumiasi,\n\
    profiilejasi ja ryhmiäsi."
design:
  intro: "## Eri tavalla suunniteltu vapauttava työkalu\n\nMobilizon on suunniteltu\
    \ käytännölliseksi työkaluksi joka kunnioittaa huomiotasi,\nautonomiaasi sekä\
    \ vapauttasi."
  attention:
    title: Huomiotasi säästäen
    md: "Mobilizon **ei ole sosiaalinen media eikä harrastus, se on työkalu**.\nMobilizonissa\
      \ ei ole seuraajien laskuria, tykkäyksiä tai äärettömästi\n<i lang=\"en\">selattavaa\
      \ sisältöä</i>.\n\nMobilizon on suunniteltu **vapauttamaan huomiosi itsesi \n\
      lavastamisen mekanismista**. \nVoit siksi keskittyä siihen millä on oikeasti\
      \ merkitystä eli tapahtumiesi hallinnointiin,\nryhmiisi ja toimintaan."
  federation:
    title: Monimuotoisuutta federaatiosta
    md: "Mobilizon on **federoitu ohjelmisto**: sen ylläpitäjät voivat asentaa sen\n\
      palvelimelle luoden instansseja jotka näyttäytyvät eri Mobilizon verkkosivuina.\n\
      Mobilizon instanssit voivat verkottua toisiinsa, eli profiili\npalvelimella\
      \ A voi osallistua ryhmään joka toimii instanssilla B.\n\nTämä monilukuisuus\
      \ **mahdollistaa monipuoliset ylläpito-olosuhteet**\n(hallinnointi, palveluehdot,\
      \ perussopimukset) ja **välttää alustan \nmonopolisoitumisen**."
  freedom:
    title: Kunnioittaa vapauttasi
    md: "Mobilizon on vapaa ohjelmisto. Se tarkoittaa että koodi on läpinäkyvää,\n\
      julkisesti tarkistettavissa ja **siinä ei ole piilotettuja ominaisuuksia**.\n\
      \nKoodi on **yhteisön tuottamaa** ja kuka tahansa voi vapaasti\ntehdä oman versionsa\
      \ ja viedä projektia omaan suuntaansa."
  go: "## Sinä päätät\n\n- Kokeile [Mobilizon demoa](@:link.mzo-demo)\n- Löydä itsellesi\
    \ sopiva [instanssi Mobilizon.org:issa](@:link.mzo)\n- Opi käyttämään (ja asentamaan)\
    \ Mobilizon [dokumentaatiomme](@:link.jmz-docs) avulla\n- Liity mukaan keskusteluun\
    \ [foorumeillamme](@:link.jmz-forum)"
who:
  title: Kuka Mobilizonin on tehnyt?
  md: "Mobilizon on **vapaa ja avoimeen lähdekoodiin perustuva ohjelmisto, jonka ylläpitäjänä\
    \ ja rahoittajana toimii Ranskalainen voittoa tavoittelematon Framasoft**.\n\n\
    Vuonna 2004 perustettu yhdistys on suuntautunut **suosittuun koulutukseen\ndigiajan\
    \ haasteissa**. Meidän pieni organisaatio (alle 40 jäsentä ja\nalle 10 työntekijää)\
    \ on tunnettu Googlen korvaavasta internetprojektista, eli\n34 eettisen, vaihtoehtoisen\
    \ verkkotyökalun ylläpidosta.\nYhdistyksemme on luonteeltaan yleishyödyllinen\
    \ ja \"rahoitettu yli 90%\nlahjoituksilla**, jotka ovat vähennettävissä verotuksessa\
    \ Ranskalaisille veronmaksajille.\n\n**Mitä enemmän ohjelmistoa käytetään ja tuetaan,\
    \ sitä enemmän se saa käyttäjiä\nja sitä useampi auttaa sen kehityksessä, eli\
    \ sitä nopeammin se kehittyy konkreettiseksi vaihtoehdoksi vastaaville palveluille,\
    \ kuten Facebookin tapahtumat tai Meetup**."
quote:
  text: Me emme voi muuttaa maailmaa Facebookilta. Vakoilukapitalismi ei tule kehittämään
    työkalua josta me unelmoimme, koska he eivät saisi siitä liikevoittoa.<br /> On
    aika luoda jotain parempaa ottamalla erilainen lähestymistapa.
  blog:
    text: Lue Framasoftin tarkoituksen pohjustus Framablogista
