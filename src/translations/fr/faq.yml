title: Quelques questions pour découvrir Mobilizon…
adoption:
  title: Ressources
  signup:
    q: Comment je fais pour m’inscrire ?
    a: |-
      Il vous faut avant tout trouver une instance de Mobilizon, c’est-à-dire
      un site web Mobilizon qu’un hébergeur a installé sur son serveur.

      Vous trouverez **notre sélection d’instances sur
      [mobilizon.org](@:link.mzo)** : notez que cette sélection est
      faite à partir de critères qui nous sont propres, et peuvent ne pas
      vous correspondre.

      Vous trouverez **un index plus complet des instances publiques Mobilizon
      [sur cette page](@:link.jmz-instances)**.

      Pour mieux choisir votre instance Mobilizon, pensez à aller regarder sa
      page « à propos » qui devrait vous éclairer sur qui propose cette
      instance, pourquoi, et dans quelles conditions.
  install:
    q: Comment installer Mobilizon sur mon serveur ?
    a: |-
      Vous trouverez la méthode que nous préconisons dans [notre documentation
      pour installer Mobilizon](@:link.jmz-docs/administration/install)
      sur votre serveur.

      Nous compléterons cette documentation au fur et à mesure que des
      contributeurs et contributrices nous aideront à maintenir d’autres méthodes
      d’installation (Docker, etc.)
  use:
    q: Comment apprendre à me servir de Mobilizon ?
    a: |-
      Vous trouverez ici [notre documentation d’utilisation de
      Mobilizon](@:link.jmz-docs/use).

      Si vous n’y trouvez pas la réponse aux questions que vous vous posez ou
      si vous pensez qu’il y manque des informations, venez en discuter avec
      nous sur [notre forum des contributions](@:link.jmz-forum).
  develop:
    q: Comment participer au code de Mobilizon ?
    a: |-
      Tout d’abord, il va falloir des connaissances en Git et en Elixir.
      Si cela ne vous dit rien, c’est que, pour l’instant, le projet n’est pas
      en capacité de recevoir vos contributions en code.

      Ensuite, il suffit d’aller sur [le dépôt du logiciel](@:git.mobilizon),
      et d’écrire une issue ou bien de le forker pour commencer à proposer ses
      propres contributions.

      Bien entendu, c’est toujours mieux de venir se parler avant, par exemple
      en utilisant [notre salon
      Matrix](https://matrix.to/#/#Mobilizon:matrix.org).
  contribute:
    q: Comment participer à Mobilizon, si je ne sais pas coder ?
    a: |-
      Le plus simple, c’est de venir parler avec nous, sur [l’espace
      dédié à Mobilizon](@:link.jmz-forum)
      dans notre forum des contributions.

      Souvenez-nous que nous ne sommes pas une multi-nationale du web, ni même
      une grosse start-up, mais une association de moins de 40 membres (qui
      mènent d’autres projets en parallèle).
      Ne vous formalisez pas si nous mettons du temps à vous répondre !
utility:
  title: Utilisation
  social:
    q: Comment promouvoir ce que je fais, ce que je suis, sur Mobilizon ?
    a: |-
      Attention : si votre objectif est la promotion, la popularité,
      la viralité, la célébrité, le buzz, etc. Mobilizon n’est probablement
      pas l’outil qu’il vous faut.

      Mobilizon est un outil qui permet de publier des événements, et de
      s’organiser en groupe (avec des discussions, un espace de liens partagés,
      et la publication de billets).

      **Mobilizon n’est pas un média social, ni un outil de communication
      virale** : il faut plutôt le considérer comme un espace d’autonomie pour
      votre groupe, dont les membres pourront suivre les nouveaux contenus.

      Si vous souhaitez populariser un événement ou le billet que votre groupe
      a publié sur Mobilizon, le mieux reste d’en partager le lien sur vos
      médias sociaux habituels, listes de diffusion, et autres moyens de
      communication sociale.
  group:
    q: En quoi Mobilizon peut m’aider à organiser mon groupe ?
    a: |-
      Le fonctionnement de Mobilizon a été pensé pour répondre aux besoins des
      groupes qui souhaitent s’organiser en interne tout en ayant une existence
      publique.

      Cela se traduit en plusieurs outils, publics et/ou internes au groupe :
      - _(public)_ une page de présentation du groupe, présentant sa
        description ainsi que ses événements et billets publics ;
      - _(public ou interne)_ un outil de publication d’événements ;
      - _(public ou interne)_ un outil de publication de billets, à la manière
        d’un blog minimaliste ;
      - _(interne)_ un outil de discussions de groupe, à la manière d’une
        catégorie de forum ;
      - _(interne)_ un annuaire des liens utiles du groupe, avec accès direct à
        des outils collaboratifs.

      Ces outils sont là pour que les aventures collectives ne se limitent pas
      à un hashtag et un rassemblement, en donnant aux bonnes volontés les
      moyens de créer du collectif et de favoriser le faire ensemble.
  outlaw:
    q: Si c’est libre, on peut y dire TOUT ce qu’on veut ? Même des trucs hors-la-loi ?
    a: |-
      Être libre ne signifie pas être au-dessus de la loi ! Chaque hébergement
      Mobilizon peut décider de ses propres conditions générales d’utilisation,
      dans le cadre de la loi dont ils dépendent.

      Par exemple, en France, les contenus niant l’holocauste sont interdits et
      peuvent être signalés aux autorités. Mobilizon permet aux internautes de
      signaler un contenu problématique aux personnes qui hébergent cette
      instance, et chaque hébergeur doit alors appliquer sa modération
      conformément à ses conditions générales et à la loi.

      Le système de fédération, quant à lui, permet aux hébergeurs de décider
      avec qui ils veulent se mettre en réseau, ou pas, selon les types de
      contenus ou les politiques de modération des autres.
  liability:
    q: Qui est responsable des contenus publiés sur Mobilizon ?
    a: |-
      Mobilizon n’est pas une plateforme centralisatrice : c’est un logiciel
      qui permet à un hébergeur de créer un site web de gestion d’événements et
      de groupes. C’est ce site web que l’on appelle une _instance Mobilizon_.

      La première personne légalement responsable est celle qui publie des
      contenus illicites ou problématiques.

      Cependant, selon les lois du pays dont dépend l’instance Mobilizon,
      l’hébergeur de cette instance peut aussi être co-responsable des contenus
      illicites.

      Par exemple, en France, tout hébergeur informé en bonne et due forme
      d’un contenu manifestement illicite en devient co-responsable s’il ne
      prend pas au plus vite les mesures pour le dépublier.
  join-group:
    q: Comment je fais pour demander à rejoindre un groupe ?
    a: |-
      Sur la version 1.0.0 de Mobilizon, il n’est pas encore possible de
      demander à rejoindre un groupe : le bouton `rejoindre un groupe` est
      grisé sur les pages publiques de chaque groupe.

      Cependant, si nous avons affiché ce bouton (ainsi qu’un message expliquant
      que _« Vous pouvez uniquement être invité aux groupes pour le moment »_),
      c’est parce que nous voulons intégrer une telle fonctionnalité.

      Le fait est que, dans un outil fédéré, coder ce bouton soulève une
      multitude de cas complexes. Par conséquent, nous avons encore besoin de
      temps pour faire les choses correctement.

      En attendant, n’hésitez pas à commenter l’événement d’un groupe pour leur
      signaler que vous désirez y être invité·e !
  attention:
    q: Pourquoi je ne peux pas suivre un profil, ni faire en sorte que mes ami·es suivent mon profil sur Mobilizon ?
    a: |-
      Parce que Mobilizon n’est pas un média social.

      Les géants du web utilisent des mécaniques qui manipulent nos egos pour
      enfermer notre attention dans leurs médias sociaux. Afficher en gros
      combien de personnes nous « suivent » flatte nos egos et nous pousse à
      nous impliquer dans la mise en scène de notre vie sur ces plateformes
      attentionnelles.

      Permettre de suivre l’activité de personnes sur un outil numérique peut
      donc avoir des conséquences désastreuses, tout en présentant un intérêt
      faible pour un outil d’organisation d’événements et de gestion de groupes.

      Dans Mobilizon, ce sont les groupes, et non les individus, qui sont au
      cœur des fonctionnalités.
politics:
  title: Politique
  facebook:
    q: Pourquoi Mobilizon ne remplace-t-il pas Facebook ?
    a: |-
      Mobilizon n’est pas pensé pour être le « Facebook killer ».

      Nous voyons bien le danger qu’il y a à publier un rassemblement militant
      sur Facebook, une plateforme monopolistique qui [multiplie les
      scandales](https://dayssincelastfacebookscandal.com) concernant la vie
      privée et la manipulation de l’opinion publique.
      Seulement, comparés à ceux des géants du web, nos moyens sont modestes.

      Notre ambition l’est tout autant : commençons par un outil qui fait peu
      mais bien. Un outil construit sur une base solide qui permettra ensuite
      d’évoluer au gré des contributions communautaires.

      Nous avons fait le choix de nous concentrer sur les besoins spécifiques
      d’un public particulier (le public militant), ce qui n’empêchera pas
      d’autres communautés d’utiliser Mobilizon dans d’autres cas.
      Ensuite, à plus long terme, nous serons en mesure d’adapter l’outil à ces
      autres publics.

      De plus, nous ne voulons pas reproduire la toxicité de Facebook. Les
      entreprises du [capitalisme de surveillance](https://mooc.chatons.org/mod/lesson/view.php?id=70)
      utilisent les mécanismes de l’économie de l’attention pour enfermer nos usages,
      capter nos comportements et nous imposer de la publicité.

      Mobilizon ne dépend pas d’un tel modèle économique : c’est l’occasion
      d’essayer de faire mieux, en faisant autrement.
  activists:
    q: C’est vrai que Mobilizon est développé pour un public militant ? Et si je ne le suis pas…?
    a: |-
      Oui, Mobilizon a été pensé comme un outil répondant en priorité aux
      besoins de militantes et militants qui cherchent à se rassembler,
      s’organiser, et se mobiliser.

      Cela signifie que nous sommes allé·es interroger des personnes militant
      d’une manière ou d’une autre pour comprendre leurs pratiques numériques et
      mieux imaginer l’outil qui correspondrait à leurs attentes.

      Si le public militant a été au cœur de nos préoccupations dans la
      conception du logiciel, cela ne veut pas dire que nous excluons les
      autres usages. Nous pensons que si un outil est assez bien conçu pour
      aider un groupe à organiser une manifestation, alors il devrait couvrir
      les besoins d’une famille organisant un anniversaire surprise :wink: !

      Dit autrement, qui que vous soyez, vous êtes libres d’utiliser Mobilizon
      comme bon vous semble.
  who:
    q: Qui développe quoi dans Mobilizon ?
    a: |-
      Mobilizon est développé par Framasoft, une association française
      d’éducation populaire aux enjeux du numérique.

      Elle emploie un développeur, tcit, qui depuis 2018 travaille sur
      l’ensemble des projets nécessaires à Mobilizon :

      - Développement de nouvelles fonctionnalités et [maintenance du
        logiciel](@:git.mobilizon) ;
      - Réflexions sur l’architecture, le design, et le futur de Mobilizon (en
        partenariat avec la designer @:people.mcgodwin) ;
      - Préparation de nouvelles versions stables et communication (billets
        de blog, changelog, etc.) ;
      - Création et maintenance de [l’index des instances
        Mobilizon](@:link.jmz-instances) ;
      - Création et maintenance du site de [documentation de
        Mobilizon](@:link.jmz-docs) ;
      - Support et discussions sur les bugtrackers, le forum, le channel Matrix
        ou par email des projets Mobilizon ;
      - Revues de code et intégration des pull requests/merge requests, ainsi
        que support des fonctionnalités ajoutées par des contributeurs externes
        (corrections de bugs, documentation, factorisation, etc.) ;
      - Maintenance de plusieurs instances Mobilizon.

      tcit travaille de surcroît sur des projets internes à Framasoft
      (Framagenda, Framapiaf, admin-sys, etc.). Dit autrement, la gestion des
      projets nécessaires à Mobilizon est assurée par un seul salarié, qui a
      en plus d’autres responsabilités à assurer dans sa charge de travail.

      D’autres membres bénévoles et salarié·es de Framasoft contribuent aussi
      au projet Mobilizon, sur divers aspects (stratégie, communication,
      documentation, développement, animation de communauté). Il n’en reste pas
      moins que Mobilizon n’est pas développé par une _startup_, avec une équipe
      de 50 personnes, et un _workflow_ rempli de _process_ hyper formatés.

      Framasoft prendra donc le temps de faire avancer le logiciel petit à
      petit, à son rythme et avec ses méthodes atypiques qui, jusqu’à présent,
      se sont montrées plutôt efficaces.
  centrist:
    q: Mobilizon est un outil, cela signifie-t-il qu’il est neutre ?
    a: |-
      Non, il ne l’est pas.

      Nous croyons qu’aucun outil n’est neutre : la manière dont un outil est
      conçu (son design), ainsi que l’empreinte de cet outil dans notre culture
      commune, tout cela influence nos comportements. C’est pour ces raisons
      que, par exemple, nous avons le réflexe d’attraper un tournevis par la
      poignée.

      Mobilizon est développé par Framasoft, une association française
      d’éducation populaire aux enjeux du numérique.

      Mobilizon fait partie d’un nombre d’actions que l’association Framasoft a
      rassemblées dans sa campagne [Contributopia](@:link.cuo).
      Ces actions visent à proposer des outils numériques alternatifs à ceux
      issus du capitalisme de surveillance, pour que les personnes qui ne se
      reconnaissent pas dans un tel système puissent se créer des espaces de
      liberté.

      De ce fait, le développement de Mobilizon et/ou la gestion des sites
      associés au projet Mobilizon (joinmobilizon.org, mobilizon.org,
      instances.mobilizon.org, etc.) peuvent ne pas plaire à tout le monde
      (notamment à cause d’idées, d’idéologies ou de cultures différentes).

      Nous respectons cela, et rappelons que l’ensemble des logiciels du projet
      Mobilizon sont libres et peuvent être dupliqués et développés avec
      d’autres influences culturelles.
  others:
    q: Pourquoi avoir fait Mobilizon alors qu’il existe GetTogether / Démosphère / etc. ? Quelle est la différence entre ces outils ?
    a: |-
      Avant de nous engager dans deux ans de travail pour développer Mobilizon,
      nous avons pris le soin de regarder ce qui se faisait ailleurs, et de nous
      demander s’il ne serait pas mieux de contribuer à un projet existant.

      Il existe plusieurs autres outils de gestion d’événements et groupes assez
      intéressants, mais nous n’en avons trouvé aucun qui remplisse tous nos critères.

      Nous voulions un logiciel libre, fédéré selon le protocole ActivityPub,
      fonctionnel à grande échelle, conçu hors de l’économie de l’attention, et
      pouvant répondre aux usages d’un public militant. Un tel logiciel
      n’existait pas, donc nous l’avons fait.

      Nous ne prétendons pas par ailleurs que Mobilizon est mieux que l’un ou
      l’autre de ces outils. Il correspond à un public, à un besoin.
      Nous croyons aussi fermement que la diversité des projets est une bonne chose.
      Le capitalisme nous a appris à penser : « un désir, un besoin, une marque »,
      qu’il s’agisse d’un soda ou dune petite feuille jaune amovible auto-adhésive.
      Nous pensons que chaque outil a sa place, et nous ne cherchons pas à faire
      de Mobilizon un outil dominant.

      Enfin, nous considérons que la [coopétition](https://fr.wikipedia.org/wiki/Coop%C3%A9tition)
      entre outils libres et fédérés sera à terme bénéfique pour établir des normes,
      des protocoles et des usages plus robustes et résilients.
  trademarks:
    q: Est-ce que «&nbsp;Mobilizon&nbsp;» est une marque déposée&nbsp;?
    a: |-
      Oui, Framasoft détient la marque déposée «&nbsp;Mobilizon&nbsp;» dans plusieurs
      pays à des fins préventives.

      Framasoft affirme ici son intention de ne pas utiliser la propriété intellectuelle
      à des fins prédatrices ou pour contraindre une utilisation légitime du logiciel.
      En revanche, Framasoft se réserve le droit de faire respecter le dépôt de
      la marque, spécifiquement en cas d'atteinte à l'image du projet «&nbsp;Mobilizon&nbsp;».
      Nous vous recommandons de créer votre propre identité numérique, sans
      utiliser «&nbsp;Mobilizon&nbsp;» pour votre projet (nom du logiciel, nom
      de domaine...) afin de ne pas semer la confusion sur qui est derrière le site web/projet.

      Pour en savoir plus, merci de vous référer à notre [Code de conduite
      concernant les marques](https://framasoft.org/fr/trademarks/).
concepts:
  title: Concepts
  instance:
    q: C’est quoi une instance ?
    a: |-
      Lorsqu’une personne installe Mobilizon sur son serveur, elle crée une
      _instance_ de Mobilizon. Concrètement, il s’agit d’un _site web_ généré
      et géré par le logiciel Mobilizon.

      Ce site web Mobilizon permet à des personnes de créer des comptes, des
      groupes, des événements, etc. : les données ainsi créées se nichent alors
      sur le disque dur du serveur de l’instance Mobilizon. C’est pour cela qu’on
      dit aussi qu’il s’agit d’un _hébergement_.

      On peut comparer ces hébergements à des locaux, comme une maison des
      associations ou un immeuble d’appartements. Chaque personne se créant un
      compte installe ses affaires (ses données, ses contenus) dans l’appartement.
      Chaque groupe créé prend possession, en quelque sorte, d’une des salles communes.

      Les utilisateurs et utilisatrices de ces locaux peuvent avoir l’impression
      de s’approprier les lieux… mais concrètement, le propriétaires des murs,
      celui qui est en capacité d’instaurer un règlement intérieur et de le
      faire respecter, c’est l’hébergeur.
  host:
    q: C’est quoi un hébergeur ?
    a: |-
      L’hébergeur désigne la personne, ou plus souvent le groupe de personnes
      (association, entreprise, collectif, etc.) qui propose un service en ligne.
      Dans la pratique, ces personnes disposent d’un serveur sur lequel elles
      installent un logiciel (par exemple le logiciel Mobilizon).
      Cette installation du logiciel, appelée instance, est accessible en
      ligne par le public, sous la forme d’un site web Mobilizon.

      Le public peut donc accéder à cette instance et interagir avec, par
      exemple en se créant un compte, en publiant des événements, en discutant
      dans un groupe.
      Toutes ces actions sont autant d’informations numériques, donc des
      données qui sont hébergées sur le disque dur du serveur de l’hébergeur.

      On peut comparer un hébergeur web à un propriétaire qui mettrait ses
      locaux à disposition du public. Son rôle est à la fois d’assurer le bon
      fonctionnement et l’entretien des infrastructures (administration du système),
      mais aussi de mettre en place et de faire respecter un règlement intérieur
      (modération, administration), voire d’assurer les bonnes relations avec
      les propriétaires voisins s’il décide de se fédérer avec eux (politique
      de fédération).
  fediverse:
    q: C’est quoi, le fédiverse ?
    a: |-
      Le fediverse _(ou la fédiverse, ou le fedivers, c’est comme vous voulez,
      en fait)_ désigne l’ensemble des logiciels, des hébergements et des données
      qui pourraient interagir ensemble, parce qu’elles parlent la même langue,
      c’est-à-dire le protocole Activity Pub.

      En d’autres termes, le fediverse rassemble tous les comptes, contenus,
      instances et outils qui sont en capacité de se retrouver au sein d’une
      même fédération.

      Attention, il faut distinguer fediverse et fédération. Plusieurs logiciels
      utilisent le protocole Activity Pub : PeerTube, Mastodon, Mobilizon,
      FunkWhale, etc.

      Ces logiciels parlent le même langage et sont _théoriquement_ en capacité
      d’interagir ensemble : ils font donc partie du fediverse.

      Dans la _pratique_, ces interactions n’en sont qu’à leurs débuts, car il
      faut les imaginer, puis les implémenter dans le code de ces logiciels,
      puis diffuser ce bout de code en mettant à jour l’installation de ces
      logiciels sur le serveur des hébergeurs.

      Lorsque les hébergeurs permettent que leur instance de tel logiciel
      interagisse avec l’instance d’un autre hébergeur, alors ces deux instances
      interconnectées forment un petit bout de fédération.

      Au sein du fédiverse, il n’y a donc pas _une_ fédération mais de
      nombreuses _bulles de fédération_, plus ou moins isolées ou interconnectées.
