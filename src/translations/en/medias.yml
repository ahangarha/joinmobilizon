title: Media Area
press:
  title: Press Release
  md: |-
    #### Mobilizon, a free and federated tool to get our events off Facebook!

    On Tuesday, May 14, 2019, the non-profit organization <a href="https://framasoft.org/en/"
    target="_blank">Framasoft</a> launched a fundraiser for the <a href="https://joinmobilizon.org/en/"
    target="_blank">Mobilizon software</a>. You will find here elements to understand
    its context and discover functionalities.

    #### Why Mobilizon?

    During the <a href="https://contributopia.org/en/">Contributopia
    campaign</a>, Framasoft announced that we’d provide a new web service that will
    allow communities to create their own event publication spaces, in order to
    make it easier to emancipate themselves from the tech giants platforms. Indeed,
    when we organize gatherings through Facebook tools or on Meetup-like services,
    we provide group members private data to these Big Tech’s platforms. Clicking
    "Going" on a Facebook event is problematic: it says <a href="https://digitalcontentnext.org/wp-content/uploads/2018/08/DCN-Google-Data-Collection-Paper.pdf"
    target="_blank">much more about yourself than you might think</a>, gives <a
    href="https://www.theguardian.com/news/series/cambridge-analytica-files">significant
    power to advertisers who pay Facebook</a> and encloses the community events
    into <a href="https://www.ted.com/talks/zeynep_tufekci_how_the_internet_has_made_social_change_easy_to_organize_hard_to_win"
    target="_blank">a tool that will prevent it from self-managing</a>. We should
    not forget that terms and conditions of use on these platforms allow their owners
    to close a group or a community without any reason whatsoever. Moreover, their
    centralized structure represents an entry point for intelligence agencies and
    malicious hackers.

    #### Mobilizon, a free and federated alternative

    Mobilizon will be ***a free software respecting fundamental freedoms***
    for people who want to gather together. Mobilizon will be <strong><em>a digital
    common</em></strong> which respects privacy and activism by design. Mobilizon
    will be ***a user-friendly, emancipatory and ethical tool***.

    Because we want to offer the software the most suited to the needs of the users,
    we took time to think about what a tool that would really empower individuals
    and groups might look like. That’s why we’ve spent the last few months analysing
    existing tools to understand their various features and to consider how we could
    respect users by deploying them. It also seemed essential to us to study the
    digital practices of activists to understand their needs for gathering, organizing
    and mobilizing. That’s why we worked with two designers: <a href="http://mcpaccard.com/"
    target="_blank">Marie-Cécile Paccard</a> and <a href="https://designandhuman.com/"
    target="_blank">Geoffrey Dorne</a>.

    Moreover, we want Mobilizon to be ***federated*** in order
    to allow communities to install their own instance (event publication website).
    Mobilizon instances will be able to connect to each other. This will help to
    foster interactions between communities and their users. As the <a href="https://en.wikipedia.org/wiki/ActivityPub"
    target="_blank">ActivityPub</a> federation protocol allows interaction between
    software applications using the same protocol, We can imagine Mobilizon being
    linked to [Mastodon](https://joinmastodon.org/) (alternative
    to Twitter), [PeerTube](https://joinpeertube.org/en/)
    (alternative to YouTube) and many other similar tools.

    #### Mobilizon, the fundraising

    Framasoft have invested time, work and money to imagine Mobilizon’s design.
    Since our organization is financed only by donations, we set up this fundraising
    to keep investing in the development of this software . We have chosen to organize
    it on our own website in order to reduce bank and administrative costs to the
    minimum. Thus our aim is to propose a new model for financing digital commons.

    Launched on Tuesday, May 14, 2019, the Mobilizon fundraising is organized around
    3 levels to reach before July 10:

    ##### Level 1: €20,000 - Free & basic version

    This amount will cover our prototype expenses. Thus, we will recover the initial
    investment for design and promotion of Mobilizon. Namely: the functionalities
    benchmark, the designers work on uses and interfaces, and the development time
    on basic functionalities on events.

    → This 1st level was reached in less than 5 days.

    ##### Level 2: €35,000 – Federated version

    With this extra €15,000, we will be able to finance ActivityPub protocol implementation
    and the development of administration tools for instances. The fact that Mobilizon
    is federated is important to allow users to connect to several communities.
    Each Mobilizon account could be linked to other federated softwares.

    ##### Level 3: €50,000 – Ideal Version

    If we reach €50,000 in donations, we will get involved in design and development
    of new features. Thus, we would like Mobilizon to provide a groups feature,
    which would integrate messaging and exchange tools between members. Another
    feature that seems necessary to us is the possibility of integrating external
    tools which community members already use (survey tools, decision-making tools,
    collaborative tools, etc.). Finally, we intend to integrate the possibility
    for Mobilizon users to manage several identities depending on the communities
    they are part of.

    With €50,000, we would have the resources to produce the best software we can
    create until fall of 2019. Then, we will publish a beta release built on a solid
    base which will allow us to offer you more features. With extra cash, we could
    consider going beyond this beta release. The users feedback and comments on
    this beta release will allow us to improve Mobilizon in order to propose a first
    complete release in the first half of 2020. Producing Mobilizon is not a sprint
    race, where you promise everything, right away, to everyone. It’s more like
    a cross-country race whose first step is to develop a tool which will allow
    you to create events and do it well.

    The success of this fundraising depends on each of us. Framasoft is committed
    to broadcast Mobilizon’s progress at each new step, on the <a href="https://joinmobilizon.org/en/news/"
    target="_blank">News page</a> and in the <a href="https://framalistes.org/sympa/info/mobilizon-newsletter"
    target="_blank">dedicated newsletter</a>.

    #### Framasoft

    [Framasoft](https://framasoft.org/) is a non-profit
    organisation created in 2004, which focuses on popular education on the stakes
    of our digital world. Our small organisation (less than 40 members, including
    9 employees) is known to have carried out the project <a href="https://degooglisons-internet.org/en/"
    target="_blank">De-google-ify Internet</a>, which proposes 34 ethical and alternative
    online tools. Since 2017, we have been developing <a href="https://joinpeertube.org/en/"
    target="_blank">PeerTube</a>, a free and federated alternative to YouTube. Recognised
    for its service of public interest, our organisation is more than 90% funded
    through donations, mainly from our French supporters.

    #### Contact us

    Would you like more information about the Mobilizon fundraising or to conduct
    an interview? Feel free to contact us via medias@framasoft.org.

    #### Relay our fundraising on your social medias

    - **Share the URL of our fundraising website:** [@:link.joinmobilizon](@:link.joinmobilizon)
    - **2 texts proposals for accompanying text:**

    > A free and federated alternative to organize our events off Facebook, Meetup
      and others?<br /> #Mobilizon, the new #Framasoft project needs you!!!

    > A free software who allows communities to create their own events publishing
      spaces to better emancipate themselves from the tech giants’ platforms?<br />
      #Mobilizon, the new #Framasoft project needs you!!!

    - **Hashtag ideas to go with your posts:**<br /> #Mobilizon #JoinMobilizon
      #Framasoft #DigitalCommon #EmancipatoryTool #FundRaising #fediverse #ActivityPub
visual:
  title: Visuals
mockup:
  title: Mockup
speakabout:
  title: Press review
  intro: Here are some extracts from our <a href="https://wiki.framasoft.org/speakabout">Press
    release</a>.
  empty_lang: You can find articles in other languages below.
  th1: Date
  th2: Media
  th3: Links
  lArticle: Link to the article
  lPaywall: Link to the article (subscribers)
  lSummary: Link to the summary of the issue
  lPodcast: Link to the podcast
  lListen: Link to the listening page
about:
  title: Useful information
  list: |-
    - [Contact us](@:link.contact/#questions) (This form relates directly
      to our press service).
    - <a href="https://framalistes.org/sympa/info/mobilizon-newsletter">Newsletter
      subscription page</a>.
    - [Wikipedia page](https://en.wikipedia.org/wiki/Framasoft) dedicated
      to Framasoft.
    - Framasoft [Complete presentation](@:link.soutenir/association).
    - Framasoft [Press release](https://wiki.framasoft.org/speakabout).
  desc: |-
    Framasoft is a freely available public education network which aims to promote
    and spread free software and free culture.

    As a link between the world of free software and the general public, our association
    offers many projects (directory software, a publisher, a websearch engine, etc.),
    with three main goals: Free Software, Free Culture, and Free Services.'
